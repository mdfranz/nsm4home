# What is this?

I hope to write a blog to provide more context but if I don't...

This is a very simple `hello-world` NSM using [Suricata](https://suricata-ids.org/) to capture DNS and TLS activity (either on a single host or tap/span) and extract events using [Vector](https://vector.dev) and then forward them to Kafka for AWS SQS.

The simple python script polls either Kafka or SQS and prints out new records. 

# Using it

Run Suricata (modify interface, obviously)

```
mfranz@franz-E6230:~$ docker run --rm -it --net=host     --cap-add=net_admin --cap-add=sys_nice -v $(pwd)/logs:/var/log/suricata jasonish/suricata:latest -i wlp2s0
Checking for capability sys_nice: yes
Checking for capability net_admin: yes
1/1/2021 -- 20:37:27 - <Notice> - This is Suricata version 6.0.1 RELEASE running in SYSTEM mode
```

Update `eve.toml` and run vector to send events to SQS/Kafka


```
mfranz@franz-E6230:~$ vector -c eve.toml 
Jan 01 15:39:29.968  INFO vector::app: Log level is enabled. level="vector=info,codec=info,file_source=info,tower_limit=trace,rdkafka=info"
Jan 01 15:39:29.970  INFO vector::app: Loading configs. path=[("eve.toml", None)]
Jan 01 15:39:30.032  INFO vector::topology: Running healthchecks.
Jan 01 15:39:30.033  INFO vector::topology: Starting source. name="eve"
Jan 01 15:39:30.033  INFO vector::topology: Starting transform. name="json"
Jan 01 15:39:30.033  INFO vector::topology: Starting transform. name="tls"
Jan 01 15:39:30.033  INFO vector::topology: Starting transform. name="stats"
Jan 01 15:39:30.033  INFO vector::topology: Starting transform. name="dns"
Jan 01 15:39:30.033  INFO vector::topology: Starting sink. name="dns_file"
Jan 01 15:39:30.033  INFO vector::topology: Starting sink. name="stats_file"
Jan 01 15:39:30.033  INFO vector::topology: Starting sink. name="kafka"
Jan 01 15:39:30.033  INFO vector::topology: Starting sink. name="tls_file"
Jan 01 15:39:30.033  INFO vector: Vector has started. version="0.11.1" git_version="v0.11.1" released="Thu, 17 Dec 2020 17:03:52 +0000" arch="x86_64"
Jan 01 15:39:30.033  INFO vector::internal_events::api: API server running. address=127.0.0.1:8686 playground=http://127.0.0.1:8686/playground
Jan 01 15:39:30.033  INFO source{component_kind="source" component_name=eve component_type=file}: vector::sources::file: Starting file server. include=["/home/mfranz/logs/eve.json"] exclude=[]
Jan 01 15:39:30.034  INFO vector::topology::builder: Healthcheck: Passed.
Jan 01 15:39:30.037  INFO vector::topology::builder: Healthcheck: Passed.
Jan 01 15:39:30.037  INFO vector::topology::builder: Healthcheck: Passed.
Jan 01 15:39:30.038  INFO source{component_kind="source" component_name=eve component_type=file}:file_server: file_source::checkpointer: Loaded checkpoint data.
Jan 01 15:39:30.039  INFO source{component_kind="source" component_name=eve component_type=file}:file_server: vector::internal_events::file::source: Resuming to watch file. path="/home/mfranz/logs/eve.json" file_position=69492715
Jan 01 15:39:30.047  INFO vector::topology::builder: Healthcheck: Passed.
```

Poll the events. This only handles DNS QUERIES and SNI's extracted from Suricata

```
mfranz@franz-E6230:~/gitlab/nsm4home/app$ ./poll.py kafka suricata localhost:9092
['./poll.py', 'kafka', 'suricata', 'localhost:9092']
0) NEW SNI api.twitter.com
1) NEW DNS verizon.net
2) NEW DNS chat-pa.clients6.google.com
3) NEW DNS clients4.google.com
4) NEW DNS connectivity-check.ubuntu.com
5) NEW DNS oauthaccountmanager.googleapis.com
6) NEW DNS google.com
7) NEW DNS cnn.com
8) NEW DNS turner-tls.map.fastly.net
9) NEW SNI data.cnn.com
10) NEW DNS clients6.google.com
11) NEW DNS amazon.com
```

# What I left out
- Running [Kafka on Docker](https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html)
- Creation of SQS Queue in AWS and normal IAM Dance

# Tested Versions
- Vector v0.11.1
- Confluent Kafka,Zookeper, etc. 6.0.1 from https://github.com/confluentinc/cp-all-in-one/blob/6.0.1-post/cp-all-in-one/docker-compose.yml
- Python 3.8 with `boto3` 1.16.30 and `kafka-python` 2.0.2
- Docker version 20.10.1, build 831ebea (amd64 but Suricata does work on arm64 as well)

# References
- https://vector.dev/
- https://github.com/jasonish/docker-suricata
- https://github.com/confluentinc/cp-all-in-one
