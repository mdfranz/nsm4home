BENTHOS_INTEL="https://github.com/Jeffail/benthos/releases/download/v3.41.1/benthos_3.41.1_linux_amd64.tar.gz"
BENTHOS_ARM="https://github.com/Jeffail/benthos/releases/download/v3.41.1/benthos_3.41.1_linux_arm64.tar.gz"

echo "Creating Suricata configuration directory"
cp -av config/suricata $HOME

echo "Creating Docker compose. Please modify"
cp -av infra/compose/suricata.yml $HOME/docker-compose.yml

sudo apt -y install python3-dev python3-pip mosh tmux bridge-utils
pip3 install docker-compose

echo -n "Use Redis> " 
read REDIS

if [[ "$REDIS" != "" ]]
then
  sudo apt-get -y install redis-server
  cp ~/suricata/suricata-redis.yaml ~/suricata/suricata.yaml
fi


if docker pull hello-world
then
	docker-compose -f ~/docker-compose.yml pull
else
  echo "Docker is not installed"
fi

cd 

wget https://github.com/Jeffail/benthos/releases/download/v3.41.1/benthos_3.41.1_linux_amd64.tar.gz

echo -n "Use Bridge> " 
read BRIDGE

if [[ "$BRIDGE" != "" ]]
then
	sudo cp config/rc.local/rc.local /etc/
	sudo cp config/rc.local/rc-local.service /etc/systemd/system/
fi

