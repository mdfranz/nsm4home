#!/usr/bin/env python3

# pip3 install asyncio-nats-client

import asyncio
from datetime import datetime
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout

class Client:
    def __init__(self, nc, loop=asyncio.get_event_loop()):
        self.nc = nc
        self.loop = loop

    async def message_handler(self, msg):
        print(f"[Received on '{msg.subject}']: {msg.data.decode()}")

    async def request_handler(self, msg):
        print("[Request on '{} {}']: {}".format(msg.subject, msg.reply, msg.data.decode()))

        await self.nc.publish(msg.reply, b"I can help!")

    async def start(self,myservers=["nats://192.168.2.200:4222"]):
        try:
            await self.nc.connect(servers=myservers, loop=self.loop)
        except:
            pass

        nc = self.nc

        try:
            sid = await nc.subscribe("suricata.dns", "", self.message_handler)
            #await nc.auto_unsubscribe(sid, 10)

        except ErrConnectionClosed:
            print("Connection closed prematurely")

        if nc.is_connected:
            await nc.subscribe("help", "workers", self.request_handler)

            try:
                start_time = datetime.now()
                response = await nc.timed_request("help", b'help please',0.5)
                end_time = datetime.now()
                await nc.flush(0.500)
            except ErrTimeout:
                print("[Error] Timeout!")

            await asyncio.sleep(2, loop=self.loop)
            await nc.close()

        if nc.last_error is not None:
            print(f"Last Error: {nc.last_error}")

        if nc.is_closed:
            print("Disconnected.")
if __name__ == '__main__':
    c = Client(NATS())
    c.loop.run_until_complete(c.start())
    c.loop.close()
