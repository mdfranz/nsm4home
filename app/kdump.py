#!/usr/bin/env python3

import json,sys,pickle,logging,platform
from pathlib import Path
from kafka import KafkaConsumer

class DomainCache:
  """Wrapper for Pickles to save seen domains"""
  def __init__(self,fname="/var/tmp/nsm4home.pkl"):
    self.debug = True

    if Path(fname).is_file():
      with open(fname,"rb") as f:
        self.data = pickle.load(f)

        if self.debug:
          print ("Found {} domains in cache".format(len(self.data)))
    else:
      self.data = set()
    self.fname = fname

  def add(self, domain, tag=None):
    """Take SNI/A record and strip"""

    if domain.find('.') > 0:
      if domain not in self.data:
        self.data.add(domain)
        if self.debug:
          print("ADDING "+domain)
          print (tag)

  def has_domain(self,domain):
    return domain in self.data

  def save(self):
    with open(self.fname,"wb") as f:
      pickle.dump(self.data,f)

class EventQueue:
  """Simple queue wrapper on SQS or Kafka"""

  def __init__(self,qn="suricata-dns",loc="us-east-2"):
    self.queue = KafkaConsumer("suricata-"+qn,bootstrap_servers=[loc],group_id="dump-"+platform.node())

  def get_messages(self):
    return self.queue # this is a consumer

def strip_host(h):
  return '.'.join( h.split('.')[-2:] )

def process_message(m,c,t):
  j = json.loads(m)
  etyp = j['event_type']
  dip = j['dest_ip']
  sip = j['src_ip']
  ts = j['timestamp']
  host = j['host']
  tags = (host,ts,sip)

  if etyp != "flow":  
    print (host,sip,dip,ts)

  if t in j:
    if t == "tls":
      print(j['tls']['sni'])
    if t == "tls":
      print(j['tls']['sni'])
    if t == "flow":
      if j['flow']['state'] == 'new':
        print (host,sip,dip,ts)
    if t == 'dns':
      if j['dns']['type'] == 'query':
        print(j['dns']['rrname'])
  else:
    print(j[t])

if __name__ == "__main__":
  if len(sys.argv) > 1:
    events = EventQueue(qn=sys.argv[1],loc=sys.argv[2])
    c = DomainCache()

    try:
      # I guess this is the place to put it
      logger = logging.getLogger('kafka')
      logger.setLevel(logging.DEBUG)
      logfmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
      fh = logging.FileHandler("/tmp/kdump.log")
      fh.setFormatter(logfmt)
      logger.addHandler(fh)

      for message in events.get_messages():
        process_message(message.value,c,sys.argv[1])
        c.save()
    except KeyboardInterrupt:
        print ("Exiting... saving cache")
        c.save()
    except Exception:
      traceback.print_exc(file=sys.stdout)
      sys.exit(0)
  else:
    print ("Usage")
    print ("\tpoll.py topic-suffix> <borker>")
    
    
