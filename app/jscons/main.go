package main

import (
	// "context"
	"fmt"
	"log"
	"os"
	"strconv"
       "time"

	"github.com/buger/jsonparser"
	"github.com/nats-io/nats.go"
)

func dump_event(m []byte) {
	event_type, _ := jsonparser.GetString(m, "event_type")
	// fmt.Printf("%s\t", event_type)

	if event_type == "flow" {
		dst_ip, _ := jsonparser.GetString(m, "dest_ip")
		fmt.Printf("DST: %s\n", dst_ip)
	} else if event_type == "tls" {
		tls_event, _, _, _ := jsonparser.Get(m, "tls")
		tls_sni, _ := jsonparser.GetString(tls_event, "sni")
		fmt.Printf("SNI: %s\n", tls_sni)
	} else if event_type == "dns" {
		dns_event, _, _, _ := jsonparser.Get(m, "dns")
		dns_type, _ := jsonparser.GetString(dns_event, "type")
		if dns_type == "query" {
			dns_rrname, _ := jsonparser.GetString(dns_event, "rrname")
			fmt.Printf("QUERY: %s\n", dns_rrname)
		}
	}
}

func main() {

	if len(os.Args) < 3 {
		fmt.Println("jscons <nats-url> <subject pattern> <consumer> <batch-size>")
		os.Exit(1)
	}

	nc, err := nats.Connect(os.Args[1], nats.UserInfo(os.Getenv("NATS_USER"), os.Getenv("NATS_PASSWORD")))
	if err != nil {
		panic(err)
	}

	js, _ := nc.JetStream()

	// subject, consumer
	sub, err := js.PullSubscribe(os.Args[2], os.Args[3])

	if err != nil {
		log.Println("pull sub: ", err)
	}

	batch, err := strconv.Atoi(os.Args[4])

	if err != nil {
		panic(err)
	}

	for {
		//msgs, err := sub.Fetch(batch, nats.Context(context.Background()))
    // See https://pkg.go.dev/github.com/nats-io/nats.go#PullOpt
		// msgs, err := sub.Fetch(batch, nats.Context(context.Background()))
                msgs, err := sub.Fetch(batch, nats.MaxWait(2*time.Second))
		if err != nil {
			log.Println("sub fetch: ", err)
			break
		}

		for _, msg := range msgs {
			dump_event(msg.Data)
			_ = msg.Ack()
		}
	}

/*
	sig := make(chan os.Signal, 1)
	log.Println("running...")
	<-sig
	log.Println("stop")
*/
}
