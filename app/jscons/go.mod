module example.com/jscons

go 1.15

require (
	github.com/buger/jsonparser v1.1.1
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.3.0 // indirect
	github.com/nats-io/nats.go v1.11.0
	google.golang.org/protobuf v1.27.0 // indirect
)
