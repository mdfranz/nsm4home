#!/usr/bin/env python3

import pika, sys, os, json

def strip_host(h):
  return '.'.join( h.split('.')[-2:] )

def process_message(m):
  j = json.loads(m)

  etyp = j['event_type']
  dip = j['dest_ip']
  sip = j['src_ip']
  ts = j['timestamp']
  host = j['host']
  tags = (host,ts,sip)

  if etyp == "tls":
    e = j['tls']
    if 'sni' in e:
      print(strip_host(e['sni']),tags)
  elif etyp == "dns":
    e = j['dns']
    if e['type'] == "query":
      if 'rrname' in e:    
        print(strip_host(e['rrname']),tags)

def callback(ch, method, properties, body):
  process_message(body.decode())

if __name__ == '__main__':
  try:
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=sys.argv[1]))
    channel = connection.channel()
    channel.basic_consume(queue=sys.argv[2], on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


  except KeyboardInterrupt:
    print('Interrupted')
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)
