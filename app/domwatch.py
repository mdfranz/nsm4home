#!/usr/bin/env python3

import json,sys,pickle
from pathlib import Path

try:
  import boto3
except Exception as e:
  print (str(e))

try:
  from kafka import KafkaConsumer
except Exception as e:
  print (str(e))

class DomainCache:
  """Wrapper for Pickles to save seen domains"""
  def __init__(self,fname="/var/tmp/nsm4home.pkl"):
    self.debug = True

    if Path(fname).is_file():
      with open(fname,"rb") as f:
        self.data = pickle.load(f)

        if self.debug:
          print ("Found {} domains in cache".format(len(self.data)))
    else:
      self.data = set()
    self.fname = fname

  def add(self, domain, tag=None):
    """Take SNI/A record and strip"""

    if domain.find('.') > 0:
      if domain not in self.data:
        self.data.add(domain)
        if self.debug:
          print("ADDING "+domain)
          print (tag)
      else:
        if self.debug:
          #print("FOUND "+domain)         
          pass 

  def has_domain(self,domain):
    return domain in self.data

  def save(self):
    with open(self.fname,"wb") as f:
      pickle.dump(self.data,f)

class EventQueue:
  """Simple queue wrapper on SQS or Kafka"""

  def __init__(self,qt="sqs",qn="topic",loc="us-east-2"):
    if qt == "sqs":
      s = boto3.resource("sqs",region_name=loc) 
      self.queue = s.get_queue_by_name(QueueName=qn)
    elif qt == "kafka":
      self.queue = KafkaConsumer(qn,bootstrap_servers=[loc])
    self.type = qt

  def get_messages(self):
    if self.type == "sqs":
      return self.queue.receive_messages()
    elif self.type == "kafka":
      return self.queue # this is a consumer

def strip_host(h):
  return '.'.join( h.split('.')[-2:] )

def process_message(m,c):
  j = json.loads(m)
  etyp = j['event_type']
  dip = j['dest_ip']
  sip = j['src_ip']
  ts = j['timestamp']
  host = j['host']
  tags = (host,ts,sip)

  if etyp == "tls":
    e = j['tls']
    if 'sni' in e:
      c.add(strip_host(e['sni']),tags)
  elif etyp == "dns":
    e = j['dns']
    if e['type'] == "query":
      if 'rrname' in e:    
        c.add(strip_host(e['rrname']),tags)

if __name__ == "__main__":
  if len(sys.argv) > 1:
    events = EventQueue(qt=sys.argv[1],qn=sys.argv[2],loc=sys.argv[3])
    c = DomainCache()
    try:
      if events.type == "sqs":
        while True:
          for message in events.get_messages():
            process_message(message.body,c)
            message.delete()
      elif events.type == "kafka":
          for message in events.get_messages():
            process_message(message.value,c)
      c.save()

    except KeyboardInterrupt:
      print ("Exiting... saving cache")
      c.save()
    except Exception:
      traceback.print_exc(file=sys.stdout)
      sys.exit(0)
  else:
    print ("Usage")
    print ("\tpoll.py <sqs|kafka> <queue|topic> <region|bootstrap server>")
