from airflow.models.dag import DAG
from airflow.providers.amazon.aws.sensors.s3_key import S3KeySensor, S3KeySizeSensor
from airflow.operators.bash import BashOperator
from datetime import datetime, timedelta

yday = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

default_args = {
  'owner': 'airflow',
  'depends_on_past': False,
  'start_date': yday,
  'email': ['mdfranz@gmail.com'],
  'email_on_failure': False,
  'email_on_retry': False,
  'retries': 1,
  'retry_delay': timedelta(minutes=5)
}

dag = DAG('s3_file_sensor', default_args=default_args)

t1 = S3KeySensor(
  task_id='s3_file_test',
  aws_conn_id='aws',
  wildcard_match=True,
  poke_interval=0,
  timeout=3,
  soft_fail=True,
  bucket_key='s3://suricata-mfranz/fileinfo/*.json',
  bucket_name=None,
  dag=dag)

t2 = BashOperator(
  task_id='task2',
  depends_on_past=False,
  bash_command='echo a big hadoop job putting files on s3',
  trigger_rule='all_failed',
  dag=dag)

t3 = BashOperator(
  task_id='task3',
  depends_on_past=False,
  bash_command='echo im next job using s3 files',
  trigger_rule='all_done',
  dag=dag)
 
t2 << t1
t3 << t2
