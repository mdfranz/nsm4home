# NATS Server

## Binary

```
sudo mkdir -p /confluent/{kafka-data,zk-data,zk-txn-logs}
sudo chown -R 1000:1000 /confluent
```


# Configuring Streams

# References
- https://docs.confluent.io/platform/current/quickstart/cos-quickstart.html
- https://docs.confluent.io/platform/current/installation/docker/operations/external-volumes.html
