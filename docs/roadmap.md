# Current State (Feb 2021)
## Suricata
- Log IDS to file or Redis
- Execution through Docker/Docker-Compose
- Support Intel, ARM (32/64 bit) with 1GB Memory
- Default Suricata configuration for rules (bu 

## Pipelines
- Per `event_type` topic distribution to Kafka
- Selected events to SQS or Kafka

## Application Tooling
- Monitor for new domains (SQS, Kafka) 
- Dump (Kafka) 
