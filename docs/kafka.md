# What is included?
Confluent Community Versions of Kafka, Zookeeper with Kafdrop UI

# Preparation
(Assumes Docker compose installed)

*Hardware Requirements*

Recommend minimum of 4GB VM/Physical Server for running Kafka and 2-4 Cores (only amd64 at the moment)

*Creating External Volumes* (Required for persistent storage)

```
sudo mkdir -p /confluent/{kafka-data,zk-data,zk-txn-logs}
sudo chown -R 1000:1000 /confluent
```

# Tweaks of Docker-Compose File (Required)

Update the following environment variables to add the host IP address:
- `KAFKA_ADVERTISED_LISTENERS` in broker
- `KAFKA_BROKERCONNECT` in kafdrop

# Start the Containers

Assuming you want persistent storage, us this

```
docker-compose -f confluent-kafka-ui-persistent.yml up -d
```

Or not

```
docker-compose -f confluent-kafka-ui.yml up -d
```

You should see something like this with `docker ps`

```
59225c57f567   obsidiandynamics/kafdrop          "/kafdrop.sh"            9 minutes ago   Up 8 minutes   0.0.0.0:9000->9000/tcp                                                     kafdrop
585a02cd97cb   confluentinc/cp-kafka:6.0.1       "/etc/confluent/dock…"   9 minutes ago   Up 8 minutes   0.0.0.0:9092->9092/tcp, 0.0.0.0:9101->9101/tcp, 0.0.0.0:29092->29092/tcp   broker
20f411933d8e   confluentinc/cp-zookeeper:6.0.1   "/etc/confluent/dock…"   9 minutes ago   Up 8 minutes   2888/tcp, 0.0.0.0:2181->2181/tcp, 3888/tcp                                 zookeeper
```

`CAVEAT`: these don't have any security (authentication/encryption) for either broker communication or the UI

# References
- https://docs.confluent.io/platform/current/quickstart/cos-quickstart.html
- https://docs.confluent.io/platform/current/installation/docker/operations/external-volumes.html
